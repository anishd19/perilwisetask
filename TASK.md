This is a repository created and maintained by Perilwise Insurance Broking Pvt Ltd.

Welcome!  My apologies in advance for the lack of formatting (this repository was put up rather hastily)

We were very impressed with your credentials and would like to take your application forward to the next round.

This round is designed to give us an idea of your overall coding expertise. Where not explicitly stated, you can choose to use any platform that is most comfortable to you.

You are required to build a dashboard for a company's backend operations

a.	Server side coding: ALL APIS HAVE TO BE WRITTEN IN PYTHON-FLASK

b.	Database:  You are free to use any database.

However, please update why you used the database you used in the section below. Also, provide a list of all DBs that you considered

Database Used:

Databases Considered:

Reason for selecting the database:

c.	Login – You should have a login enabled application with the following credentials: username: testuser password: testuser
The landing page should be publicly viewable while the dashboard should have login based access.  You have complete control on design of the landing page

d.	Views in the front end: You have complete control on how you want to design the front end.  The only constraints we place is that you use material design where possible.  You are free to design the front end app in whatever way you see fit


e.	Functional items in the views:  We would like you to design the following functional items in the respective pages

Dashboard - A dashboard for the viewer with the following functions:

1. business – Has to be default View

2. Customer Support


Business:

Business should have two sections:
1. Generate in excel - Import the dummy data from the policyData sheet in data.xlsx and put it in the database.  You should show the data in a data table and give the user the ability to export the report back in excel
2. Business numbers – Visualize premiums earned by type of policy in a graphic form. With monthly and weekly views.

Customer support:

Data:

1. claimData – Contains mock claims that have been generated for the policies issued

Functionality:
1.	You should design a simple claims management system - A data table should show claims (from the claimData sheet)
2.	A functional close button that should change the status of a claim from “open” to “close” and send a mail to the customer’s email ID stating that the claim has been closed
3.	For claims that have been closed, a button that will allow us to re-open the claim (claimStatus has to be toggled)
